let tabs = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content > li");

tabs.forEach(function(tab, index){
  tab.addEventListener("click", function(){
    tabs.forEach(function(tab){
      tab.classList.remove("active");
    })
    tab.classList.add("active");

    tabsContent.forEach(function(content){
      content.style.display = "none";
    });

    tabsContent[index].style.display = "block";
  })
})